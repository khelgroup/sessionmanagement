'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let sessionSchema = new Schema({
    sessionId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    playerId: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ''
    },
}, { versionKey: false  })


mongoose.model('Session', sessionSchema);

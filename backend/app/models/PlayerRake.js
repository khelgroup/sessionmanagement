/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let playerRake = new Schema({
    playerRakeId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    playerId: {
        type: String,
        default: ''
    },
    competitionId: {
        type: String,
        default: ''
    },
    amount: {
        type: Number,
        default: 0
    },
    productId: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ""
    }
});
mongoose.model('PlayerRake', playerRake);

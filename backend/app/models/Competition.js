'use strict'
/**
 * Module Dependencies
 */
const appConfig = require("./../../config/appConfig");
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let competitionSchema = new Schema({
    competitionId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    competitionName: {
        type: String,
        default: ''
    },
    competitionType: {
        type: String,
        default: ''
    },
    competitionCategory: {
        type: String,
        default: ''
    },
    rankingType: {
        type: String,
        default: ''
    },
    winningPercent: {
        type: String,
        default: ''
    },
    // removing it due to duplication
    // gameName: {
    //   type: String,
    //   default: ''
    // },
    gameId: {
        type: String,
        default: ''
    },
    // timeLimit: {
    //   type: String,
    //   default: ''
    // },
    playerLimit: {
        type: Number,
        default: 0
    },
    entryType: {
        type: String,
        default: ''
    },
    winningType: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    entryAmount: {
        type: Number,
        default: 0
    },
    winningAmount: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        default: ''
    },
    winningBreakUp: [],
    productId: {
        type: String,
        default: ''
    },
    startTime: {
        type: Date,
        default: ""
    },
    endTime: {
        type: Date,
        default: ""
    },
    // players: [],
    scores: [],
    ranks: [],
    createdOn: {
        type: Date
    }
}, { usePushEach: true });
mongoose.model('Competition', competitionSchema);
'use strict'
/**
 * Module Dependencies
 */
const appConfig = require("./../../config/appConfig");
const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let competitionSchema = new Schema({
  competitionRakeId: {
    type: String,
    default: '',
    // enables us to search the record faster
    index: true,
    unique: true
  },
  competitionId:{
    type: String,
    default: ''
  },
  rakeAmount: {
    type: Number,
    default: 0
  },
  rakePercent: {
    type: Number,
    default: 0
  },
  totalAmount: {
    type: Number,
    default: 0
  },
  productId: {
    type: String,
    default: ''
  },
  createdOn :{
    type:Date,
    default:""
  }


})


mongoose.model('CompetitionRake', competitionSchema);

// module.exports = {
//   CompetitionRakeModel: mongoose.createConnection(appConfig.db.gameEngineUri).model('CompetitionRake',competitionSchema)
// };
const express = require('express');
const router = express.Router();
const budgetController = require("./../../app/controllers/budgetController");
// const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig")

const middleware = require('../middlewares/auth');
module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/budget`;

    // defining routes.

    // Manager Side
    // create Budget(password, firstName, role)
    app.post(`${baseUrl}/createBudget`, budgetController.createBudget);

    app.post(`${baseUrl}/createPlayerRake`, budgetController.createPlayerRake);

    // // create user Monthly budget(teamMemberId, amount, date)
    // app.post(`${baseUrl}/createUserMonthlyBudget`, middleware.isAuthorize,budgetController.createUserMonthlyBudget);
    //
    // // get Budget List
    // app.get(`${baseUrl}/getBudget`, middleware.isAuthorize, budgetController.getBudgetList);
    //
    // // get User Monthly Budget List
    // app.get(`${baseUrl}/getUserMonthlyBudgetList`, middleware.isAuthorize ,budgetController.getUserMonthlyBudgetList);
    //
    // // update Budget(Date, amount)
    // app.post(`${baseUrl}/updateBudget`,middleware.isAuthorize , budgetController.updateBudget);
    //
    // // update User Monthly Budget(Date, amount)
    // app.post(`${baseUrl}/updateUserMonthlyBudget`, middleware.isAuthorize, budgetController.updateUserMonthlyBudget);
    //
    //
    // //(Team Member side)
    //
    // // get per User(Team Member) Budget
    // app.get(`${baseUrl}/getperUserBudget`, middleware.isAuthorize, budgetController.getperUserBudget);

}

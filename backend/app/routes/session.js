const express = require('express');
const router = express.Router();
const sessionController = require("./../../app/controllers/sessionController");
const appConfig = require("./../../config/appConfig");
const middleware = require('../middlewares/auth');

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/session`;

//NEW
    // get All Session
    app.post(`${baseUrl}/getSession`, sessionController.getSession);

    app.post(`${baseUrl}/getExcelSessionList`, sessionController.getExcelSessionList);
}

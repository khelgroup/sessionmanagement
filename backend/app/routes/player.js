const express = require('express');
const router = express.Router();
const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig");
const middleware = require('../middlewares/auth');

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/players`;

    // this api use for user login with role 'player'

    // create session with Player Id
    app.post(`${baseUrl}/createSession`, playerController.createSession);

    // create session with Player Id
    app.post(`${baseUrl}/createPlayer`, playerController.createPlayer);

// create session with Player Id
    app.get(`${baseUrl}/Test`, playerController.test);

}

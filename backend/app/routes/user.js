const express = require('express');
const router = express.Router();
const userController = require("./../../app/controllers/userController");
const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig")

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/users`;

    // defining routes.

    // get all player in one list by pagination
    app.get(`${baseUrl}/getplayerlist`, userController.getPlayerList);

    // get Details of player in Dashboard(DAU, MAU, ARPU, ALT, ALTV)
    app.get(`${baseUrl}/getDetails`, userController.getDetails);

    // get Details of player in Dashboard(DAU, MAU, ARPU, ALT, ALTV)
    app.post(`${baseUrl}/getMonthlyDetails`, userController.getMonthlyDetails);

    // get Details of player in Dashboard(DAU, MAU, ARPU, ALT, ALTV)
    app.post(`${baseUrl}/getRakeDetails`, userController.getRakeDetails);

    // get Details of player in Dashboard(DAU, MAU, ARPU, ALT, ALTV)
    app.post(`${baseUrl}/getActivePlayer`, userController.getActivePlayer);

    // get Details of player in Dashboard(DAU, MAU, ARPU, ALT, ALTV)
    app.post(`${baseUrl}/getExpenses`, userController.getExpenses);

    // create user for system(password, firstName, role)
    app.post(`${baseUrl}/createUser`, userController.createUser);
}

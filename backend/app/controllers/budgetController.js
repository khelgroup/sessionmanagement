const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
// const logging = require('./../libs/logging');
const validateInput = require('../libs/paramsValidationLib')
const check = require('../libs/checkLib')
const {isNullOrUndefined} = require('util');
const tokenLib = require('../libs/tokenLib')

const moment = require('moment')
var cron = require('node-cron');

var nodeMailer = require('nodemailer');


/* Models */
const UserModel = mongoose.model('User');
const Budget = mongoose.model('Budget');
const PlayerRake = mongoose.model('PlayerRake');
const CompetitionRake = mongoose.model('CompetitionRake');
// const userMonthlyBudget = mongoose.model('UserMonthlyBudget');

// create Budget function
let createBudget = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.budgetAmount && req.body.date) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "budgetAmount or date missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let checkManager = () => {
        console.log("checkManager");
        return new Promise((resolve, reject) => {
            UserModel.find({userId: req.body.managerId}, function (err, managerDetails) {
                if (err) {
                    // logging.captureError(500, "error", err, null);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(managerDetails)) {
                    // logging.captureError(500, "Manager not found", 'Manager not found', null);
                    let apiResponse = response.generate(true, "Manager not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve();
                }
            });
        });
    } // end of checkManager function

    let checkBudget = () => {
        console.log("checkBudget");
        return new Promise((resolve, reject) => {
            let date = new Date(req.body.date);
            date = (date.getFullYear()) +'-'+(date.getMonth() + 1);
            Budget.find({$and: [{createdBy: req.body.managerId}, {month: date}]}, function (err, budgetDetails) {
                if (err) {
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(budgetDetails)) {
                    resolve();
                } else {
                    let apiResponse = response.generate(true, "Budget is already created for that month", 500, null);
                    reject(apiResponse);
                }
            });
        });
    } // end of checkBudget function

    let createBudget = () => {
        console.log("createBudget");
        return new Promise((resolve, reject) => {
            let body = {};
            body['budgetId'] = shortid.generate();
            body['date'] = new Date(req.body.date);
            let date = new Date(req.body.date);
            body['month'] = (date.getFullYear()) +'-'+(date.getMonth() + 1);
            body['budgetAmount'] = req.body.budgetAmount;
            body['createdBy'] = req.body.managerId;
            body['createdOn'] = new Date();
            Budget.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create Budget", "budgetController => createBudget()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createBudget function

    validatingInputs()
        .then(checkManager)
        .then(checkBudget)
        .then(createBudget)
        .then((resolve) => {
            // logging.captureError(200, "success", 'Budget Created Successfully!!', resolve);
            let apiResponse = response.generate(false, "Budget Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            // logging.captureError(500, "error", err, null);
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of create Budget function

// update Budget function
// let updateBudget = (req, res) => {
//
//     let validatingInputs = () => {
//         console.log("validatingInputs");
//         return new Promise((resolve, reject) => {
//             if (req.body.budgetId && req.body.twoDates) {
//                 resolve(req);
//             } else {
//                 let apiResponse = response.generate(true, "budgetId missing", 400, null);
//                 reject(apiResponse);
//             }
//         });
//     }; // end of validatingInputs
//
//     let checkBudget = () => {
//         console.log("checkBudget");
//         return new Promise((resolve, reject) => {
//             if((req.body.twoDates).length > 1){
//                 let firstDate = new Date(req.body.twoDates[0]).getMonth()
//                 let secondDate = new Date(req.body.twoDates[1]).getMonth()
//                 if(firstDate === secondDate){
//                     resolve()
//                 }else{
//                     logger.error("You cannot change the month", "budgetController => checkBudget()", 5);
//                     let apiResponse = response.generate(true, "You cannot change the month", 500, null);
//                     reject(apiResponse);
//                 }
//             }else{
//                 resolve()
//             }
//         });
//     } // end of checkBudget function
//
//     let updateBudgets = () => {
//         console.log("updateBudgets");
//         return new Promise((resolve, reject) => {
//             let json = {}
//             if (req.body.date) {
//                 let date = new Date(req.body.date);
//                 let month = (date.getFullYear()) +'-'+(date.getMonth() + 1);
//                 json = {
//                     'date': date,
//                     'budgetAmount': req.body.budgetAmount,
//                     'month': month
//                 }
//             }
//             Budget.findOneAndUpdate({budgetId: req.body.budgetId}, json, {new: true})
//                 .select('-__v -_id')
//                 .exec((err, budget) => {
//                     if (err) {
//                         logger.error("Failed to retrieve budget data", "budgetController => updateBudgets()", 5);
//                         let apiResponse = response.generate(true, "Failed to retrieve budget", 500, null);
//                         reject(apiResponse);
//                     } else if (check.isEmpty(budget)) {
//                         logger.error("Failed to update budget", "budgetController => updateBudgets()", 5);
//                         let apiResponse = response.generate(true, "Failed to update budget", 500, null);
//                         reject(apiResponse);
//                     } else {
//                         logger.info("budget found", "budgetController => updateBudgets()", 10);
//                         resolve(budget);
//                     }
//                 })
//         });
//     } // end of updateePromoters function
//
//     validatingInputs()
//         .then(checkBudget)
//         .then(updateBudgets)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "Updated Budget Successfully!!", 200, resolve);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of update Budget function
//
// // get Budget List function
// let getBudgetList = (req, res) => {
//
//     // let getSizeLimit = () => {
//     //     console.log("getSizeLimit");
//     //     return new Promise((resolve, reject) => {
//     //         let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
//     //         if (req.query.pg > 0) {
//     //             skip = (limit) * (req.query.pg)
//     //         }
//     //         let data = {}
//     //         data['skip'] = skip;
//     //         data['limit'] = limit;
//     //         resolve(data)
//     //     });
//     // } // end of getSizeLimit function
//
//     let findBudget = () => {
//         console.log("findBudget");
//         return new Promise((resolve, reject) => {
//             Budget.find({createdBy: req.data.managerId}, {}, {
//                 sort: {createdOn: 1}
//             })
//                 .select('-__v -_id')
//                 .exec((err, budgetDetails) => {
//                     if (err) {
//                         logger.error("Failed to retrieve budget", "budgetController => findBudget()", 5);
//                         let apiResponse = response.generate(true, "Failed to retrieve budget", 500, null);
//                         reject(apiResponse);
//                     } else if (check.isEmpty(budgetDetails)) {
//                         logger.error("No budget found", "budgetController => findBudget()", 5);
//                         let apiResponse = response.generate(true, "No budget found", 500, null);
//                         reject(apiResponse);
//                     } else {
//                         /*let final = [];
//                         playersDetails.forEach((item) => {
//                             let finalObject = item.toObject();
//                             delete finalObject._id;
//                             delete finalObject.__v;
//                             final.push(finalObject)
//                         })*/
//                         resolve(budgetDetails);
//                     }
//                 });
//         });
//     } // end of findBudget Functions
//
//     let totalBudget = (final) => {
//         console.log("totalBudget");
//         return new Promise((resolve, reject) => {
//             Budget.find({createdBy: req.data.managerId}).count(function (err, cnt) {
//                 if (err) {
//                     logger.error("Failed to retrieve budget", "budgetController => totalBudget()", 5);
//                     let apiResponse = response.generate(true, "Failed to retrieve budget", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     final['totalRecords'] = cnt;
//                     resolve(final);
//                 }
//             });
//         });
//     } // end of total Budget function
//
//     findBudget()
//         .then(totalBudget)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "Get Budget List Successfully!!", 200, resolve);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of get Budget List function
//
// // create User Monthly Budget function
// let createUserMonthlyBudget = (req, res) => {
//
//     let validatingInputs = () => {
//         console.log("validatingInputs");
//         return new Promise((resolve, reject) => {
//             if (req.body.date && req.body.amount && req.body.teamMemberId) {
//                 resolve(req);
//             } else {
//                 let apiResponse = response.generate(true, "teamMemberId, date or Amount missing", 400, null);
//                 reject(apiResponse);
//             }
//         });
//     }; // end of validatingInputs
//
//     let checkTeamMember = () => {
//         console.log("checkTeamMember");
//         return new Promise((resolve, reject) => {
//             UserModel.find({$and: [{managerId: req.data.managerId}, {teamMemberId: req.body.teamMemberId}]}, function (err, userDetails) {
//                 if (err) {
//                     logger.error("Failed to find manager and teamMember", "budgetController => checkTeamMember()", 5);
//                     let apiResponse = response.generate(true, err, 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(userDetails)) {
//                     logger.error("Manager or teamMember not found", "budgetController => checkTeamMember()", 5);
//                     let apiResponse = response.generate(true, "Manager or teamMember not found", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     resolve(userDetails);
//                 }
//             });
//         });
//     } // end of checkManager function
//
//     let findBudget = () => {
//         console.log("findBudget");
//         return new Promise((resolve, reject) => {
//             let date = new Date(req.body.date);
//             date = (date.getFullYear()) +'-'+(date.getMonth() + 1);
//             Budget.find({$and: [{createdBy: req.data.managerId}, {month: date}]}, function (err, budgetDetails) {
//                 if (err) {
//                     logger.error("Failed to find budget", "campaignController => findBudget()", 5);
//                     let apiResponse = response.generate(true, err, 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(budgetDetails)) {
//                     logger.error("Budget is Not allocated.", "campaignController => findBudget()", 5);
//                     let apiResponse = response.generate(true, "Budget is Not allocated.", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     resolve();
//                 }
//             });
//         });
//     } // end of findBudget function
//
//     let checkUserBudget = () => {
//         console.log("checkUserBudget");
//         return new Promise((resolve, reject) => {
//             let date = new Date(req.body.date);
//             date = (date.getFullYear()) +'-'+(date.getMonth() + 1);
//             userMonthlyBudget.find({$and: [{createdBy: req.data.managerId}, {month: date}, {teamMemberId: req.body.teamMemberId}]}, function (err, budgetDetails) {
//                 if (err) {
//                     logger.error("Failed to find User budget", "budgetController => checkUserBudget()", 5);
//                     let apiResponse = response.generate(true, err, 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(budgetDetails)) {
//                     resolve();
//                 } else {
//                     logger.error("User Budget is already created for that month", "budgetController => checkUserBudget()", 5);
//                     let apiResponse = response.generate(true, "User Budget is already created for that month", 500, null);
//                     reject(apiResponse);
//                 }
//             });
//         });
//     } // end of getAllMonthlyBudget function
//
//     let getAllMonthlyBudget = () => {
//         console.log("getAllMonthlyBudget");
//         return new Promise((resolve, reject) => {
//             let date = new Date(req.body.date);
//             date = (date.getFullYear()) +'-'+(date.getMonth() + 1);
//             userMonthlyBudget.aggregate([
//                 {$match: {$and: [{createdBy: req.data.managerId}, {month: date}]}},
//                 {
//                     $group: {
//                         _id: "$createdBy",
//                         totalamount: {$sum: "$amount"}
//                     }
//                 }
//             ], function (err, budgetDetails) {
//                 if (err) {
//                     logger.error("Failed to find User Monthly budget", "budgetController => getAllMonthlyBudget()", 5);
//                     let apiResponse = response.generate(true, err, 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(budgetDetails)) {
//                     resolve();
//                 } else {
//                     resolve(budgetDetails);
//                 }
//             });
//         });
//     } // end of getAllMonthlyBudget function
//
//     let checkBudget = (budgetDetails) => {
//         console.log("checkBudget");
//         return new Promise((resolve, reject) => {
//             let budgetDetailsAmount;
//             if (budgetDetails !== undefined) {
//                 budgetDetailsAmount = (budgetDetails[0].totalamount) + Number(req.body.amount);
//             } else {
//                 budgetDetailsAmount = Number(req.body.amount);
//             }
//             let date = new Date(req.body.date);
//             date = (date.getFullYear()) +'-'+(date.getMonth() + 1);
//             Budget.find({$and: [{createdBy: req.data.managerId}, {month: date}, {budgetAmount: {$lte: budgetDetailsAmount - 1}}]}, function (err, budget) {
//                 if (err) {
//                     logger.error("Failed to find budget", "budgetController => checkBudget()", 5);
//                     let apiResponse = response.generate(true, err, 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(budget)) {
//                     resolve();
//                 } else {
//                     logger.error("Amount is extending the monthly Limit.", "budgetController => checkBudget()", 5);
//                     let apiResponse = response.generate(true, "Amount is extending the monthly Limit.", 500, null);
//                     reject(apiResponse);
//                 }
//             });
//         });
//     } // end of checkBudget function
//
//     let createBudget = () => {
//         console.log("createBudget");
//         return new Promise((resolve, reject) => {
//             let body = {};
//             body['userMonthlyBudgetId'] = shortid.generate();
//             body['date'] = req.body.date;
//             let date = new Date(req.body.date);
//             // console.log('date', date)
//             body['month'] = (date.getFullYear()) +'-'+(date.getMonth() + 1);
//             body['amount'] = req.body.amount;
//             body['teamMemberId'] = req.body.teamMemberId;
//             body['createdBy'] = req.data.managerId;
//             body['createdOn'] = new Date();
//             userMonthlyBudget.create(body, function (err, responsee) {
//                 if (err) {
//                     logger.error("Failed to create User Budget", "budgetController => createBudget()", 5);
//                     let apiResponse = response.generate(true, err, 500, null);
//                     reject(apiResponse);
//                 } else {
//                     let finalObject = responsee.toObject();
//                     delete finalObject.__v;
//                     resolve(finalObject);
//                 }
//             });
//         });
//     } // end of createBudget function
//
//     validatingInputs()
//         .then(checkTeamMember)
//         .then(findBudget)
//         .then(checkUserBudget)
//         .then(getAllMonthlyBudget)
//         .then(checkBudget)
//         .then(createBudget)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "User Budget Created Successfully!!", 200, resolve);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of create User Monthly Budget function
//
// // update User Monthly Budget function
// let updateUserMonthlyBudget = (req, res) => {
//
//     let validatingInputs = () => {
//         console.log("validatingInputs");
//         return new Promise((resolve, reject) => {
//             if (req.body.userMonthlyBudgetId && req.body.teamMemberId) {
//                 resolve(req);
//             } else {
//                 let apiResponse = response.generate(true, "userMonthlyBudgetId or teamMemberId missing", 400, null);
//                 reject(apiResponse);
//             }
//         });
//     }; // end of validatingInputs
//
//     let checkManagerandBudget = () => {
//         console.log("checkManagerandBudget");
//         return new Promise((resolve, reject) => {
//             userMonthlyBudget.find({$and: [{createdBy: req.data.managerId},{teamMemberId: req.body.teamMemberId}, {userMonthlyBudgetId: req.body.userMonthlyBudgetId}]}, function (err, budgetDetails) {
//                 if (err) {
//                     logger.error("Failed to monthly Budget", "budgetController => checkManagerandBudget()", 5);
//                     let apiResponse = response.generate(true, err, 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(budgetDetails)) {
//                     logger.error("No User Budget found", "budgetController => checkManagerandBudget()", 5);
//                     let apiResponse = response.generate(true, "No User Monthly Budget found", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     resolve();
//                 }
//             });
//         });
//     } // end of check Manager and Budget function
//
//     let checkUserBudget = () => {
//         console.log("checkUserBudget");
//         return new Promise((resolve, reject) => {
//             if((req.body.twoDates).length > 1){
//                 let firstDate = new Date(req.body.twoDates[0]).getMonth()
//                 let secondDate = new Date(req.body.twoDates[1]).getMonth()
//                 if(firstDate === secondDate){
//                     resolve()
//                 }else{
//                     logger.error("You cannot change the month", "budgetController => checkUserBudget()", 5);
//                     let apiResponse = response.generate(true, "You cannot change the month", 500, null);
//                     reject(apiResponse);
//                 }
//             }else{
//                 resolve()
//             }
//         });
//     } // end of checkUserBudget function
//
//     let updateUserBudgets = () => {
//         console.log("updateUserBudgets");
//         return new Promise((resolve, reject) => {
//             let json = {}
//             if (req.body.date) {
//                 let date = new Date(req.body.date);
//                 let month = (date.getFullYear()) +'-'+(date.getMonth() + 1);
//                 json = {
//                     'date': date,
//                     'amount': req.body.amount,
//                     'month': month
//                 }
//             }
//             userMonthlyBudget.findOneAndUpdate({userMonthlyBudgetId: req.body.userMonthlyBudgetId}, json, {new: true})
//                 .select('-__v -_id')
//                 .exec((err, userMonthlybudget) => {
//                     if (err) {
//                         logger.error("Failed to retrieve user monthly budget data", "budgetController => updateUserBudgets()", 5);
//                         let apiResponse = response.generate(true, "Failed to retrieve user monthly budget data", 500, null);
//                         reject(apiResponse);
//                     } else if (check.isEmpty(userMonthlybudget)) {
//                         logger.error("Failed to update User monthly budget", "budgetController => updateUserBudgets()", 5);
//                         let apiResponse = response.generate(true, "Failed to update User monthly budget", 500, null);
//                         reject(apiResponse);
//                     } else {
//                         // logger.info("User budget found", "budgetController => updateUserBudgets()", 10);
//                         resolve(userMonthlybudget);
//                     }
//                 })
//         });
//     } // end of update User Budgets function
//
//     validatingInputs()
//         .then(checkManagerandBudget)
//         .then(checkUserBudget)
//         .then(updateUserBudgets)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "Updated User Budget Successfully!!", 200, resolve);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of update User Monthly Budget function
//
// // get User Monthly Budget List function
// let getUserMonthlyBudgetList = (req, res) => {
//
//     let getSizeLimit = () => {
//         console.log("getSizeLimit");
//         return new Promise((resolve, reject) => {
//             let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
//             if (req.query.pg > 0) {
//                 skip = (limit) * (req.query.pg)
//             }
//             let data = {}
//             data['skip'] = skip;
//             data['limit'] = limit;
//             resolve(data)
//         });
//     } // end of getSizeLimit function
//
//     let findBudget = (data) => {
//         console.log("findBudget");
//         return new Promise((resolve, reject) => {
//             userMonthlyBudget.find({createdBy: req.data.managerId}, {}, {
//                 skip: data.skip,
//                 limit: data.limit,
//                 sort: {createdOn: 1}
//             })
//                 .select('-__v -_id')
//                 .exec((err, userMonthlyDetails) => {
//                     if (err) {
//                         logger.error("Failed to retrieve user Monthly budget", "budgetController => findBudget()", 5);
//                         let apiResponse = response.generate(true, "Failed to retrieve user Monthly budget", 500, null);
//                         reject(apiResponse);
//                     } else if (check.isEmpty(userMonthlyDetails)) {
//                         logger.error("No user Monthly budget found", "budgetController => findBudget()", 5);
//                         let apiResponse = response.generate(true, "No user Monthly budget found", 500, null);
//                         reject(apiResponse);
//                     } else {
//                         let final = [];
//                         userMonthlyDetails.forEach((item) => {
//                             let finalObject = item.toObject();
//                             delete finalObject._id;
//                             delete finalObject.__v;
//                             final.push(finalObject)
//                         })
//                         final.userBudget = final
//                         resolve(final);
//                     }
//                 });
//         });
//     } // end of findBudget Functions
//
//     let findUser = (data) => {
//         console.log("findUser");
//         return new Promise((resolve, reject) => {
//             let id = [];
//             (data.userBudget).filter((x) => {
//                 id.push(x.teamMemberId)
//             })
//             UserModel.find({teamMemberId: {$in: id}}, {}, {})
//                 .exec((err, userDetails) => {
//                     if (err) {
//                         logger.error("Failed to retrieve Team member", "budgetController => findUser()", 5);
//                         let apiResponse = response.generate(true, "Failed to retrieve Team member", 500, null);
//                         reject(apiResponse);
//                     } else if (check.isEmpty(userDetails)) {
//                         logger.error("No Team member found", "budgetController => findUser()", 5);
//                         let apiResponse = response.generate(true, "No Team member found", 500, null);
//                         reject(apiResponse);
//                     } else {
//                         let final = [];
//                         (data.userBudget).filter((x) => {
//                             userDetails.filter((y) => {
//                                 if ((x.teamMemberId).toString() === (y.teamMemberId).toString()) {
//                                     x["memberName"] = y.name
//                                     x["role"] = y.role
//                                     final.push(x)
//                                 }
//                             })
//                         })
//                         resolve(final);
//                     }
//                 });
//         });
//     } // end of findUser Functions
//
//     let totalBudget = (final) => {
//         console.log("totalBudget");
//         return new Promise((resolve, reject) => {
//             userMonthlyBudget.find({createdBy: req.data.managerId}).count(function (err, cnt) {
//                 if (err) {
//                     logger.error("Failed to retrieve budget", "budgetController => totalBudget()", 5);
//                     let apiResponse = response.generate(true, "Failed to retrieve budget", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     let json ={
//                         userBudget : final,
//                         totalRecords : cnt
//                     }
//                     resolve(json);
//                 }
//             });
//         });
//     } // end of total Budget function
//
//     getSizeLimit()
//         .then(findBudget)
//         .then(findUser)
//         .then(totalBudget)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "Get User Budget List Successfully!!", 200, resolve);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of get User Monthly Budget List function
//
// // get per User Budget function
// let getperUserBudget = (req, res) => {
//
//     let findUserBudget = () => {
//         console.log("findUserBudget");
//         return new Promise((resolve, reject) => {
//             userMonthlyBudget.find({teamMemberId: req.data.teamMemberId}, {}, {})
//                 .select('-__v -_id')
//                 .exec((err, userDetails) => {
//                     if (err) {
//                         logger.error("Failed to retrieve user Monthly Budget", "budgetController => findUserBudget()", 5);
//                         let apiResponse = response.generate(true, "Failed to retrieve user Monthly Budget", 500, null);
//                         reject(apiResponse);
//                     } else if (check.isEmpty(userDetails)) {
//                         logger.error("No user Monthly budget found", "budgetController => findUserBudget()", 5);
//                         let apiResponse = response.generate(true, "No user Monthly budget found", 500, null);
//                         reject(apiResponse);
//                     } else {
//                         /*let final = [];
//                         playersDetails.forEach((item) => {
//                             let finalObject = item.toObject();
//                             delete finalObject._id;
//                             delete finalObject.__v;
//                             final.push(finalObject)
//                         })*/
//                         resolve(userDetails);
//                     }
//                 });
//         });
//     } // end of findUserBudget Functions
//
//     let totalBudget = (final) => {
//         console.log("totalBudget");
//         return new Promise((resolve, reject) => {
//             userMonthlyBudget.find({teamMemberId: req.data.teamMemberId}).count(function (err, cnt) {
//                 if (err) {
//                     logger.error("Failed to retrieve budget", "budgetController => totalBudget()", 5);
//                     let apiResponse = response.generate(true, "Failed to retrieve budget", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     final['totalRecords'] = cnt;
//                     resolve(final);
//                 }
//             });
//         });
//     } // end of total Budget function
//
//     findUserBudget()
//         .then(totalBudget)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "Get Per User Budget Successfully!!", 200, resolve);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of get per User Budget function


let createPlayerRake = (req, res) => {

    let createRake = () => {
        console.log("createRake");
        return new Promise((resolve, reject) => {
            let body = {};
            body['competitionRakeId'] = shortid.generate();
            body['rakeAmount'] = req.body.rakeAmount;
            body['playerId'] = req.body.playerId;
            body['rakePercent'] = req.body.rakePercent;
            body['totalAmount'] = req.body.totalAmount;
            body['competitionId'] = req.body.competitionId;
            body['productId'] = req.body.productId;
            body['createdOn'] = new Date();
            CompetitionRake.create(body, function (err, responsee) {
                if (err) {
                    logger.error("Failed to create Budget", "budgetController => createBudget()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = responsee.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createBudget function

    createRake()
        .then((resolve) => {
            // logging.captureError(200, "success", 'Budget Created Successfully!!', resolve);
            let apiResponse = response.generate(false, "Budget Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            // logging.captureError(500, "error", err, null);
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of create Budget function

module.exports = {
    createBudget: createBudget,
    createPlayerRake: createPlayerRake,
    // getBudgetList: getBudgetList,
    // updateBudget: updateBudget,
    // createUserMonthlyBudget: createUserMonthlyBudget,
    // getUserMonthlyBudgetList: getUserMonthlyBudgetList,
    // updateUserMonthlyBudget: updateUserMonthlyBudget,
    // getperUserBudget: getperUserBudget,
}// end exports

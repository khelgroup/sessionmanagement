const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');
const tokenLib = require('../libs/tokenLib');
var faker = require('faker');

/* Models */

const Session = mongoose.model('Session');

//Get session
let getSession = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.body.pgSize) ? Number(req.body.pgSize) : 5;
            if (req.body.pg > 0) {
                skip = (limit) * (req.body.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findSessions = (data) => {
        console.log("findSessions");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
            }
            console.log('body', body);
            Session.find(body, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: 1}
            })
                .select('-__v -_id')
                .exec((err, sessionDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve session", "sessionController => findSessions()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(sessionDetails)) {
                        logger.error("No session found", "sessionController => findSessions()", 5);
                        let apiResponse = response.generate(true, "No session found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(sessionDetails);
                    }
                });
        });
    } // end of findSessions Functions

    let totalSessionData = (sessionDetails) => {
        console.log("totalSession");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.id !== undefined) {
                    body['competitionId'] = (req.body.data.id);
                }
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
            }
            Session.find(body).count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve session", "sessionController => totalSession()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    let final = {
                        sessionDetails : sessionDetails,
                        totalRecords: cnt
                    }
                    resolve(final);
                }
            });
        });
    } // end of totalSession function

    getSizeLimit()
        .then(findSessions)
        .then(totalSessionData)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Session List Successfully!!", 200, resolve);
            console.log('apiResponse',apiResponse)
            console.log('apiResponse totalRecords',apiResponse.data)
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Session function

//get Excel Session List
let getExcelSessionList = (req, res) => {

    let findSessions = () => {
        console.log("findSessions");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
            }
            console.log('body', body);
            Session.find(body, {}, {
                sort: {createdOn: 1}
            })
                .select('-__v -_id')
                .exec((err, sessionDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve session", "sessionController => findSessions()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(sessionDetails)) {
                        logger.error("No session found", "sessionController => findSessions()", 5);
                        let apiResponse = response.generate(true, "No session found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(sessionDetails);
                    }
                });
        });
    } // end of findSessions Functions

    let totalSession = (final) => {
        console.log("totalSession");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.id !== undefined) {
                    body['competitionId'] = (req.body.data.id);
                }
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
            }
            Session.find(body).count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve session", "sessionController => totalSession()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    final['totalRecords'] = cnt;
                    resolve(final);
                }
            });
        });
    } // end of totalSession function

    findSessions()
        .then(totalSession)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Excel Session List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Excel Session List function

module.exports = {
    getSession: getSession,
    getExcelSessionList: getExcelSessionList,
}// end exports

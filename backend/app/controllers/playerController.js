const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');
const tokenLib = require('../libs/tokenLib');
var faker = require('faker');

/* Models */

const User = mongoose.model('User');
const Player = mongoose.model('Player');
const Session = mongoose.model('Session');
const Competition = mongoose.model('Competition');

//create Session
let createSession = (req, res) =>{

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.playerId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "playerId missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let createSession = () => {
        console.log("createUser");
        return new Promise((resolve, reject) => {
            let body = {};
            body['sessionId'] = shortid.generate();
            body['playerId'] = req.body.playerId;
            body['createdOn'] = new Date();
            Session.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create session", "playerController => createSession()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    resolve(response);
                }
            });
        });
    } // end of createSession function

    validatingInputs()
        .then(createSession)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Player Session Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
}
// end of create Session function

// create Player function
let createPlayer = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.password) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "password missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let createPlayers = () => {
        console.log("createPlayers");
        return new Promise((resolve, reject) => {
            let body = {};
            body['playerId'] = shortid.generate();
            body['firstName'] = req.body.firstName;
            body['password'] = req.body.password;
            body['email'] = req.body.email;
            body['createdOn'] = new Date();
            Player.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create players", "playerController => findPlayers()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createPlayers function

    validatingInputs()
        .then(createPlayers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Player Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of create Player function


let test = (req, res) =>{

    let getCompetition = () => {
        console.log("getCompetition");
        return new Promise((resolve, reject) => {
            let newDate = new Date()
            console.log('Dateeeeeeee', newDate)
            newDate.setMinutes(newDate.getMinutes() + 30)
            console.log('Date', newDate)
            Competition.find({$and:[{status: {$ne: 'expired'}},{$or:[{endTime:  {$lte: new Date()}, startTime: {$lte: new Date()}}]}]})
                .then((data)=>{
                    if (check.isEmpty(data)) {
                        logger.error("competition not found", "userController => checkcompetition()", 5);
                        let apiResponse = response.generate(true, "competition not found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(data);
                    }
                });
                /*if (err) {
                    logger.error("Failed to create competition", "userController => checkcompetition()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(managerDetails)) {
                    logger.error("competition not found", "userController => checkcompetition()", 5);
                    let apiResponse = response.generate(true, "competition not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(managerDetails);
                }*/
            // });
        });
    } // end of createPlayers function

    let updateStatus=(data)=> {
        console.log("findPlayers");
        return new Promise((resolve, reject) => {
            let Id = [];
            let set ={}
            data.filter((x)=>{
                if(x.status === 'active'){
                    Id.push(x.competitionId)
                    set = {$set: {status: 'inactive'}}
                }else if(x.status === 'inactive'){
                    Id.push(x.competitionId)
                    set = {$set: {status: 'active'}}
                }
            })
            let query = {competitionId: {$in: Id}}
            Competition.updateMany(query, set, {new: true})
                .then(() => {
                    return {statusCode: 200, body: 'success'};
                })
                .catch(err => {
                    console.log('=> an error occurred: ', err);
                    return {statusCode: 500, body: 'error'};
                });
        })
    }

    getCompetition()
        .then(updateStatus)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Player Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
}
module.exports = {
    createSession: createSession,
    createPlayer: createPlayer,
    test: test,
}// end exports

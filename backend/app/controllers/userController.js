const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
const validateInput = require('../libs/paramsValidationLib')
const check = require('../libs/checkLib')
const {isNullOrUndefined} = require('util');
const tokenLib = require('../libs/tokenLib')
var DateDiff = require('date-diff');

const moment = require('moment')

/* Models */
const UserModel = mongoose.model('User');
const Player = mongoose.model('Player');
const Session = mongoose.model('Session');
const PlayerRake = mongoose.model('PlayerRake');
const BudgetModel = mongoose.model('Budget');
const CompetitionRake = mongoose.model('CompetitionRake');
const Campaign = mongoose.model('Campaign');

// create User function
let createUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if ((req.body.role === 'Manager') ? (req.body.password && req.body.firstName && req.body.role) : (req.body.password && req.body.firstName && req.body.role && req.body.managerId)) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "Parameters missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let checkManager = () => {
        console.log("checkManager");
        return new Promise((resolve, reject) => {
            if (req.body.role === 'Team Member') {
                UserModel.find({managerId: req.body.managerId}, function (err, managerDetails) {
                    if (err) {
                        logger.error("Failed to create user", "userController => checkManager()", 5);
                        let apiResponse = response.generate(true, err, 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(managerDetails)) {
                        logger.error("Manager not found", "userController => checkManager()", 5);
                        let apiResponse = response.generate(true, "Manager not found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    } // end of createUser function

    let createUsers = () => {
        console.log("createUser");
        return new Promise((resolve, reject) => {
            let body = {};
            if (req.body.role === 'Manager') {
                body['managerId'] = shortid.generate();
            } else {
                body['managerId'] = req.body.managerId;
                body['teamMemberId'] = shortid.generate();
            }
            body['firstName'] = req.body.firstName ? req.body.firstName : '';
            body['userId'] = shortid.generate();
            body['isActive'] = true;
            body['role'] = req.body.role;
            body['password'] = req.body.password;
            body['createdOn'] = new Date();
            UserModel.create(body, function (err, responsee) {
                if (err) {
                    logger.error("Failed to create user", "userController => createUsers()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = responsee.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createUser function

    validatingInputs()
        .then(checkManager)
        .then(createUsers)
        .then((resolve) => {
            // logging.captureError(200, "success", 'User Created Successfully!!', resolve);
            let apiResponse = response.generate(false, "User Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            // logging.captureError(500, "error", err, null);
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of create User function

let getPlayerList = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findPlayers = (data) => {
        console.log("findPlayers");
        return new Promise((resolve, reject) => {
            Player.find({}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: 1}
            })
                .select('-__v -_id')
                .exec((err, playersDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playersDetails)) {
                        logger.error("No players found", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No players found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(playersDetails);
                    }
                });
        });
    } // end of findPlayers Functions

    let findSession = (playersDetails) => {
        console.log("findWallet");
        return new Promise((resolve, reject) => {
            let query = {};
            let id = [];
            playersDetails.filter((x) => {
                id.push(x.playerId);
            })
            query = {playerId: {$in: id}}
            Session.find(query, {}, {})
                .select('-__v -_id')
                .exec((err, sessionDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve Session", "userController => findSession()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve Session", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(sessionDetails)) {
                        logger.error("No Session found", "userController => findSession()", 5);
                        let apiResponse = response.generate(true, "No Session found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = [];
                        playersDetails.filter((x) => {
                            let sessiontotal = 0;
                            let session = 0;
                            x = x.toObject();
                            sessionDetails.filter((y) => {
                                if (x.playerId === y.playerId) {
                                    sessiontotal++;
                                    if (moment(y.createdOn).format('DD-MM-YYYY') === moment(new Date()).format('DD-MM-YYYY')) {
                                        session++;
                                    }
                                }
                            })
                            x['sessiontotal'] = sessiontotal;
                            x['session'] = session;
                            final.push(x);
                        })
                        let res = {
                            results: final
                        }
                        resolve(res);
                    }
                });
        });
    } // end of findSession function

    let totalLT = (playersDetails) => {
        console.log("totalLT");
        return new Promise((resolve, reject) => {
            Session.aggregate([
                {
                    $sort: {
                        createdOn: 1
                    }
                },
                {
                    $group: {
                        _id: "$playerId",
                        first: {$first: "$createdOn"},
                        last: {$last: "$createdOn"},
                    }
                }
            ], (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve session for lifeTime", "userController => totalLT()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    let lifeTime;
                    let final = [];
                    playersDetails.results.filter((y) => {
                        sessionDetail.filter((x) => {
                            const date1 = new Date(x.last)
                            let date2 = new Date(x.first);
                            var diff = new DateDiff(date1, date2);
                            let diffDays = diff.days();
                            lifeTime = parseInt(diffDays)
                            if (y.playerId === x._id) {
                                y['lifeTime'] = lifeTime
                                y['lastLogin'] = moment(x.last).format('YYYY-MM-DD')
                            }
                        })
                        final.push(y);
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of totalLT function

    let playerRack = (playersDetails) => {
        console.log("playerRack");
        return new Promise((resolve, reject) => {
            PlayerRake.aggregate([
                {
                    $group: {
                        _id: "$playerId",
                        amount: {$sum: "$amount"},
                    }
                }
            ], (err, playerRackDetail) => {
                if (err) {
                    logger.error("Failed to retrieve players rack", "playerController => playerRack()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                    reject(apiResponse);
                } else {
                    let final = [];
                    playersDetails.results.filter((y) => {
                        playerRackDetail.filter((x) => {
                            let data = x.amount;
                            if (y.playerId === x._id) {
                                y['playerRack'] = data
                            }
                        })
                        final.push(y);
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of playerRack function

    let totalPlayers = (final) => {
        console.log("totalPlayers");
        return new Promise((resolve, reject) => {
            Player.find().count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve players", "userController => totalPlayers()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                    reject(apiResponse);
                } else {
                    final['totalRecords'] = cnt;
                    resolve(final);
                }
            });
        });
    } // end of total Players function

    getSizeLimit()
        .then(findPlayers)
        .then(findSession)
        .then(totalLT)
        .then(playerRack)
        .then(totalPlayers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Player List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Player List function

let getDetails = (req, res) => {

    let todayUser = () => {
        console.log("todayUser");
        return new Promise((resolve, reject) => {
            var time = new Date();
            time.setHours(0, 0, 0, 0);
            console.log('new Date', time)
            let query = [{
                $match: {
                    "createdOn": {$gte: time}
                }
            },
                {
                    $group: {
                        _id: "$playerId",
                        date: {$last: "$createdOn"},
                        count: {$sum: 1}
                    }
                }]
            console.log('query', query);
            Session.aggregate(query, (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve session for lifeTime", "userController => totalLT()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    let final = [];
                    console.log('sessionDetail', sessionDetail.length)
                    let Info = {}
                    Info['dailyActive'] = sessionDetail.length
                    final.push(Info);
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of totalLT function

    let monthlyUser = (playersDetails) => {
        console.log("todayUser");
        return new Promise((resolve, reject) => {
            var time = new Date();
            let monthNumber = (time.getMonth() + 1)
            console.log('new Date', time, monthNumber)
            let query = [
                {$addFields: {"month": {$month: '$createdOn'}}},
                {$match: {month: monthNumber}},
                {
                    $group: {
                        _id: {month: "$month", playerId: "$playerId"},
                    }
                }
            ]
            Session.aggregate(query, (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve session for lifeTime", "userController => totalLT()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    playersDetails.results.filter((x) => {
                        x['monthlyActive'] = sessionDetail.length
                    })
                    resolve(playersDetails);
                }
            })
        });
    } // end of monthly User function

    let totalPlayers = (final) => {
        console.log("totalPlayers");
        return new Promise((resolve, reject) => {
            Player.find().count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve players", "userController => totalPlayers()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                    reject(apiResponse);
                } else {
                    final.results.filter((x) => {
                        x['totalRecords'] = cnt;
                    })
                    resolve(final);
                }
            });
        });
    } // end of total Players function

    let playerRack = (playersDetails) => {
        console.log("playerRack");
        return new Promise((resolve, reject) => {
            PlayerRake.aggregate([
                {
                    $group: {
                        _id: null,
                        amount: {$sum: "$amount"},
                    }
                }
            ], (err, playerRackDetail) => {
                if (err) {
                    logger.error("Failed to retrieve players rack", "playerController => playerRack()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                    reject(apiResponse);
                } else {
                    let final = [];
                    playersDetails.results.filter((x) => {
                        playerRackDetail.filter((y) => {
                            x['AvgRevenuePerUser'] = Number(y.amount / x.totalRecords).toFixed(2);
                        })
                        final.push(x);
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of player Rack function

    let findPlayers = (res) => {
        console.log("findPlayers");
        return new Promise((resolve, reject) => {
            Player.find({}, {}, {})
                .select('-__v -_id')
                .exec((err, playersDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playersDetails)) {
                        logger.error("No players found", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No players found", 500, null);
                        reject(apiResponse);
                    } else {
                        let response = {
                            playersDetails: playersDetails,
                            results: res.results
                        }
                        resolve(response);
                    }
                });
        });
    } // end of findPlayers Functions

    let totalLT = (playersDetails) => {
        console.log("totalLT");
        return new Promise((resolve, reject) => {
            Session.aggregate([
                {
                    $sort: {
                        createdOn: 1
                    }
                },
                {
                    $group: {
                        _id: "$playerId",
                        first: {$first: "$createdOn"},
                        last: {$last: "$createdOn"},
                    }
                }
            ], (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve session for lifeTime", "userController => totalLT()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    let lifeTime;
                    let final = [];
                    let Info = {}
                    Info['lifeTime'] = 0
                    playersDetails.playersDetails.filter((y) => {
                        sessionDetail.filter((x) => {
                            const date1 = new Date(x.last)
                            let date2 = new Date(x.first);
                            var diff = new DateDiff(date1, date2);
                            let diffDays = diff.days();
                            lifeTime = parseInt(diffDays)
                            console.log('x._id', y.playerId, x._id)
                            if (y.playerId === x._id) {
                                Info['lifeTime'] += lifeTime
                            }
                        })
                    })
                    playersDetails.results.filter((x) => {
                        x['AvgLifeTime'] = Number(Info['lifeTime'] / x.totalRecords).toFixed(2);
                        final.push(x);
                    })
                    resolve(final)
                }
            })
        });
    } // end of totalLT function

    let totalLTValue = (playersDetails) => {
        console.log("totalLT");
        return new Promise((resolve, reject) => {
            let lifeTime;
            let final = [];
            playersDetails.filter((x) => {
                x['AvgLifeTimeValue'] = 0;
                x['AvgLifeTimeValue'] = Number(x['AvgRevenuePerUser'] * x['AvgLifeTime']).toFixed(2);
                final.push(x);
            })
            resolve(final)
        });
    } // end of totalLTValue function

    todayUser()
        .then(monthlyUser)
        .then(totalPlayers)
        .then(playerRack)
        .then(findPlayers)
        .then(totalLT)
        .then(totalLTValue)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Data Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Details function

let getMonthlyDetails = (req, res) => {

    let monthlyUser = () => {
        console.log("monthlyUser");
        return new Promise((resolve, reject) => {
            let body = {};
            let time;
            let query = []
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.start && req.body.data.end) {
                    query = [
                        {$addFields: {"month": {$month: '$createdOn'}, "year": {$year: "$createdOn"}}},
                        {
                        $match: {
                            "createdOn": {
                                "$gte": new Date(req.body.data.start),
                                "$lte": new Date(req.body.data.end)
                            }
                        }
                    },
                        {
                            $group: {
                                _id: {month: "$month", year: "$year",createdOn: "$createdOn", playerId: "$playerId"}
                            }
                        }]
                }
            } else if (req.body.date !== null && req.body.date) {
                time = new Date(req.body.date);
                let monthNumber = (time.getMonth() + 1)
                let yearNumber = time.getFullYear()
                console.log('new Date', time, monthNumber)
                query = [
                    {$addFields: {"month": {$month: '$createdOn'}, "year": {$year: "$createdOn"}}},
                    {$match: {month: monthNumber, year: yearNumber}},
                    {
                        $group: {
                            _id: {month: "$month", year: "$year", playerId: "$playerId"},
                        }
                    }
                ]
            }
            Session.aggregate(query, (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve session for lifeTime", "userController => totalLT()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    let final = [];
                    console.log('monthlyActive', sessionDetail.length)
                    let Info = {}
                    let playerId = []
                    Info['monthlyActive'] = sessionDetail.length
                    sessionDetail.filter((x) => {
                        playerId.push(x._id.playerId)
                    })
                    Info['playerId'] = playerId
                    final.push(Info);
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of monthly User function

    let monthlyBudget = (final) => {
        console.log("monthlyBudget");
        return new Promise((resolve, reject) => {
            let time;
            let query = []
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.start && req.body.data.end) {
                    // if (moment(req.body.data.startDate).format('YYYY-MM') === moment(req.body.data.end).format('YYYY-MM')) {
                    query = [{
                        $match: {
                            "month": {
                                "$gte": moment(new Date(req.body.data.start)).format('YYYY-MM'),
                                "$lte": moment(new Date(req.body.data.end)).format('YYYY-MM')
                            }
                        }
                    },
                        {
                            $group: {
                                _id: {
                                    month: "$month",
                                    createdBy: "$createdBy",
                                    budgetAmount: "$budgetAmount",
                                }
                            }
                        }]
                }
                // }
            } else if (req.body.date !== null && req.body.date) {
                time = new Date(req.body.date);
                let fullMonth = time.getFullYear()+'-'+(time.getMonth() + 1)
                console.log('new Date', time)
                query = [
                    {$match: {month: fullMonth}},
                    {
                        $group: {
                            _id: {
                                month: "$month",
                                createdBy: "$createdBy",
                                budgetAmount: "$budgetAmount",
                            },
                        }
                    }
                ]
            }
            BudgetModel.aggregate(query, (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve budget", "userController => findSession()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve budget", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(sessionDetail)) {
                    final.results.filter((x) => {
                        x['monthlyBudget'] = 0;
                    })
                    resolve(final);
                } else {
                    let count = 0
                    sessionDetail.filter((y)=>{
                        final.results.filter((x) => {
                            x['monthlyBudget'] = count;
                            x['monthlyBudget'] += y._id.budgetAmount;
                            count += y._id.budgetAmount
                        })
                    })
                    resolve(final);
                }
            })
        });
        // return new Promise((resolve, reject) => {
        //     let time;
        //     if (req.body.date !== null && req.body.date) {
        //         time = new Date(req.body.date);
        //         time = (time.getFullYear()) + '-' + (time.getMonth() + 1);
        //     } else if (Object.keys(req.body.data).length > 0) {
        //         if (req.body.data.start && req.body.data.end) {
        //             time = new Date(req.body.data.startDate);
        //             time = (time.getFullYear()) + '-' + (time.getMonth() + 1);
        //         }
        //     }
        //     BudgetModel.findOne({managerId: req.body.managerId, month: time}, function (err, managerDetails) {
        //         if (err) {
        //             logger.error("Failed to create user", "userController => checkManager()", 5);
        //             let apiResponse = response.generate(true, err, 500, null);
        //             reject(apiResponse);
        //         } else if (check.isEmpty(managerDetails)) {
        //             final.results.filter((x) => {
        //                 x['monthlyBudget'] = 0;
        //             })
        //             resolve(final);
        //         } else {
        //             final.results.filter((x) => {
        //                 x['monthlyBudget'] = managerDetails.budgetAmount;
        //             })
        //             resolve(final);
        //         }
        //     });
        // });
    }// end of monthly Budget function

    let totalPlayers = (final) => {
        console.log("totalPlayers");
        return new Promise((resolve, reject) => {
            Player.find().count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve players", "userController => totalPlayers()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                    reject(apiResponse);
                } else {
                    final.results.filter((x) => {
                        x['totalRecords'] = cnt;
                    })
                    resolve(final);
                }
            });
        });
    } // end of total Players function

    let playerRevenue = (playersDetails) => {
        console.log("playerRevenue");
        return new Promise((resolve, reject) => {
            let time;
            let query1 = [];
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.start && req.body.data.end) {
                    query1 = [{
                        $match: {
                            "createdOn": {
                                "$gte": new Date(req.body.data.start),
                                "$lte": new Date(req.body.data.end)
                            }
                        }
                    },
                        {
                            $group: {
                                _id: {createdOn: "$createdOn", playerId: "$playerId", rakeAmount: {$sum: "$rakeAmount"}}
                            }
                        }]
                }
            } else if (req.body.date !== null && req.body.date) {
                time = new Date(req.body.date);
                let monthNumber = (time.getMonth() + 1)
                let yearNumber = time.getFullYear()
                console.log('new Date', time, monthNumber)
                query1 = [
                    {$addFields: {"month": {$month: '$createdOn'}, "year": {$year: "$createdOn"}}},
                    {$match: {month: monthNumber, year: yearNumber}},
                    {
                        $group: {
                            _id: {
                                month: "$month",
                                year: "$year",
                                playerId: "$playerId",
                                rakeAmount: {$sum: "$rakeAmount"}
                            },
                        }
                    }
                ]
            }
            CompetitionRake.aggregate(query1, (err, playerRackDetail) => {
                if (err) {
                    logger.error("Failed to retrieve players rack", "playerController => playerRack()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                    reject(apiResponse);
                } else {
                    let final = [];
                    playersDetails.results.filter((x) => {
                        if (playerRackDetail.length !== 0) {
                            let amnt = 0;
                            playerRackDetail.filter((y) => {
                                amnt = amnt += y._id.rakeAmount
                            })
                            x['AvgRevenuePerMonth'] = Number(amnt / x.playerId.length).toFixed(2);
                            x['rakePerMonth'] = Number(amnt).toFixed(2);

                        } else {
                            x['AvgRevenuePerMonth'] = 0;
                            x['rakePerMonth'] = 0;
                        }
                        final.push(x);
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of player Rack function

    monthlyUser()
        .then(monthlyBudget)
        .then(totalPlayers)
        .then(playerRevenue)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Monthly Data Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Details function

let getRakeDetails = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findRake = (data) => {
        console.log("findRake");
        return new Promise((resolve, reject) => {
            let time;
            let query1 = [];
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.start && req.body.data.end) {
                    query1 = [{
                        $match: {
                            "createdOn": {
                                "$gte": new Date(req.body.data.start),
                                "$lte": new Date(req.body.data.end)
                            }
                        }
                    },
                        {"$limit": data.limit},
                        {"$skip": data.skip},
                        {
                            $group: {
                                _id: {createdOn: "$createdOn", playerId: "$playerId", rakeAmount: "$rakeAmount"}
                            }
                        }]
                }
            } else if (req.body.date !== null && req.body.date) {
                time = new Date(req.body.date);
                let monthNumber = (time.getMonth() + 1)
                let yearNumber = time.getFullYear()
                console.log('new Date', time, monthNumber)
                query1 = [
                    {$addFields: {"month": {$month: '$createdOn'}, "year": {$year: "$createdOn"}}},
                    {$match: {month: monthNumber, year: yearNumber}},
                    {"$limit": data.limit},
                    {"$skip": data.skip},
                    {
                        $group: {
                            _id: {month: "$month", year: "$year", playerId: "$playerId", rakeAmount: "$rakeAmount"},
                        }
                    }
                ]
            }
            CompetitionRake.aggregate(query1, (err, RakeDetails) => {
                if (err) {
                    logger.error("Failed to retrieve Rake", "userController => findRake()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve Rake", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(RakeDetails)) {
                    logger.error("No Rake found", "userController => findRake()", 5);
                    let apiResponse = response.generate(true, "No Rake found", 500, null);
                    reject(apiResponse);
                } else {
                    let final = []
                    RakeDetails.filter((x) => {
                        let date = moment(x._id.createdOn).format('YYYY-MM-DD')
                        let time = moment(x._id.createdOn).format("hh:mm:ss a")
                        let json = {
                            date: date,
                            time: time,
                            rakeAmount: x._id.rakeAmount
                        }
                        final.push(json)
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            });
        });
    } // end of findPlayers Functions

    getSizeLimit()
        .then(findRake)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Rake Data Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Details function

let getActivePlayer = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findSession = (data) => {
        console.log("findSession");
        return new Promise((resolve, reject) => {
            let time;
            let query = []
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.start && req.body.data.end) {
                    query = [{$addFields: {"month": {$month: '$createdOn'}, "year": {$year: "$createdOn"}}},
                        {
                        $match: {
                            "createdOn": {
                                "$gte": new Date(req.body.data.start),
                                "$lte": new Date(req.body.data.end)
                            }
                        }
                    },
                        {
                            $group: {
                                _id: {month: "$month", year: "$year",createdOn: "$createdOn", playerId: "$playerId"}
                            }
                        }]
                }
            }
            else if (req.body.date !== null && req.body.date) {
                time = new Date(req.body.date);
                let monthNumber = (time.getMonth() + 1)
                let yearNumber = time.getFullYear()
                console.log('new Date', time, monthNumber)
                query = [
                    {$addFields: {"month": {$month: '$createdOn'}, "year": {$year: "$createdOn"}}},
                    {$match: {month: monthNumber, year: yearNumber}},
                    {
                        $group: {
                            _id: {month: "$month", year: "$year", playerId: "$playerId"},
                        }
                    }
                ]
            }
            Session.aggregate(query, (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve session", "userController => findSession()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    let final = [];
                    sessionDetail.filter((x) => {
                        final.push(x._id)
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of findPlayers Functions

    let findLastLogin = (playersDetails) => {
        console.log("findLastLogin");
        return new Promise((resolve, reject) => {
            Session.aggregate([
                {
                    $sort: {
                        createdOn: 1
                    }
                },
                {
                    $group: {
                        _id: "$playerId",
                        last: {$last: "$createdOn"},
                    }
                }
            ], (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve session", "userController => findLastLogin()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
                    reject(apiResponse);
                } else {
                    let lifeTime;
                    let final = [];
                    playersDetails.results.filter((y) => {
                        sessionDetail.filter((x) => {
                            const date1 = new Date(x.last)
                            if (y.playerId === x._id) {
                                y['lastLogin'] = moment(x.last).format('YYYY-MM-DD')
                            }
                        })
                        final.push(y);
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of findLastLogin function

    let findPlayers = (res) => {
        console.log("findPlayers");
        return new Promise((resolve, reject) => {
            let Id = [];
            console.log('res', res)
            res.results.filter((id) => {
                Id.push(id.playerId)
            })
            let query = {playerId: {$in: Id}}
            Player.find(query, {}, {})
                .select('-__v -_id')
                .exec((err, playersDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playersDetails)) {
                        logger.error("No players found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No players found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = []
                        playersDetails.filter((pDet) => {
                            res.results.filter((Auser) => {
                                if (Auser.playerId === pDet.playerId) {
                                    Auser['playerId'] = pDet.playerId
                                    Auser['firstName'] = pDet.firstName
                                }
                            })
                        })
                        final.push(res);
                        resolve(res);
                    }
                });
        });
    } // end of findPlayers Functions

    getSizeLimit()
        .then(findSession)
        .then(findLastLogin)
        .then(findPlayers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Active Player Data Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });

    // let findlastSession = (res) => {
    //     console.log("findlastSession");
    //     return new Promise((resolve, reject) => {
    //         let Id = [];
    //         console.log('res',res)
    //         res.results.filter((id)=>{
    //             Id.push(id.playerId)
    //         })
    //         let query = {playerId: {$in: Id}}
    //         Player.find(query,{},{})
    //             .select('-__v -_id')
    //             .exec((err, playersDetails) => {
    //                 if (err) {
    //                     logger.error("Failed to retrieve players", "playerController => findPlayers()", 5);
    //                     let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
    //                     reject(apiResponse);
    //                 } else if (check.isEmpty(playersDetails)) {
    //                     logger.error("No players found", "playerController => findPlayers()", 5);
    //                     let apiResponse = response.generate(true, "No players found", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     let final =[]
    //                     playersDetails.filter((pDet)=>{
    //                         res.results.filter((Auser)=>{
    //                             if(Auser.playerId === pDet.playerId){
    //                                 Auser['playerId'] = pDet.playerId
    //                                 Auser['firstName'] = pDet.firstName
    //                             }
    //                         })
    //                     })
    //                     final.push(res);
    //                     resolve(final);
    //                 }
    //             });
    //     });
    // } // end of findPlayers Functions
} // end of get Details function

let getExpenses = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findSession = (data) => {
        console.log("findSession");
        return new Promise((resolve, reject) => {
            let time;
            let query = []
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.start && req.body.data.end) {
                    // if (moment(req.body.data.startDate).format('YYYY-MM') === moment(req.body.data.end).format('YYYY-MM')) {
                    query = [{
                        $match: {
                            "month": {
                                "$gte": moment(new Date(req.body.data.start)).format('YYYY-MM'),
                                "$lte": moment(new Date(req.body.data.end)).format('YYYY-MM')
                            }
                        }
                    },
                        {"$limit": data.limit},
                        {"$skip": data.skip},
                        {
                            $group: {
                                _id: {
                                    month: "$month",
                                    createdBy: "$createdBy",
                                    budgetAmount: "$budgetAmount",
                                }
                            }
                        }]
                }
                // }
            } else if (req.body.date !== null && req.body.date) {
                time = new Date(req.body.date);
                let monthNumber = (time.getMonth() + 1)
                let yearNumber = time.getFullYear()
                let fullMonth = time.getFullYear()+'-'+(time.getMonth() + 1)
                console.log('new Date', time, monthNumber)
                query = [
                    {$match: {month: fullMonth}},
                    {"$limit": data.limit},
                    {"$skip": data.skip},
                    {
                        $group: {
                            _id: {
                                month: "$month",
                                createdBy: "$createdBy",
                                budgetAmount: "$budgetAmount",
                            },
                        }
                    }
                ]
            }
            BudgetModel.aggregate(query, (err, sessionDetail) => {
                if (err) {
                    logger.error("Failed to retrieve budget", "userController => findSession()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve budget", 500, null);
                    reject(apiResponse);
                } else {
                    let final = [];
                    sessionDetail.filter((x) => {
                        final.push(x._id)
                    })
                    let res = {
                        results: final
                    }
                    resolve(res);
                }
            })
        });
    } // end of findSession Functions

    // let findLastLogin = (playersDetails) => {
    //     console.log("totalLT", playersDetails);
    //     return new Promise((resolve, reject) => {
    //         let id = []
    //         let query = {};
    //         playersDetails.results.filter((x) => {
    //             id.push(x.createdBy);
    //         })
    //         query = {createdBy: {$in: id}}
    //         Campaign.find(query, {}, {})
    //             .select('-__v -_id')
    //             .exec((err, sessionDetail) => {
    //                 if (err) {
    //                     logger.error("Failed to retrieve session for lifeTime", "userController => totalLT()", 5);
    //                     let apiResponse = response.generate(true, "Failed to retrieve session", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     let lifeTime;
    //                     let final = [];
    //                     playersDetails.results.filter((y) => {
    //                         sessionDetail.filter((x) => {
    //                             const date1 = new Date(x.last)
    //                             if (y.createdBy === x.createdBy) {
    //                                 y['url'] = x.campaignUrl
    //                             }
    //                         })
    //                         final.push(y);
    //                     })
    //                     let res = {
    //                         results: final
    //                     }
    //                     resolve(res);
    //                 }
    //             })
    //     });
    // }// end of totalLT function

    let findPlayers = (res) => {
        console.log("findPlayers");
        return new Promise((resolve, reject) => {
            let Id = [];
            console.log('res', res)
            res.results.filter((id) => {
                Id.push(id.createdBy)
            })
            let query = {userId: {$in: Id}}
            UserModel.find(query, {}, {})
                .select('-__v -_id')
                .exec((err, playersDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playersDetails)) {
                        logger.error("No players found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No players found", 500, null);
                        reject(apiResponse);
                    } else {
                        // let final = []
                        playersDetails.filter((pDet) => {
                            res.results.filter((Auser) => {
                                if (Auser.createdBy === pDet.userId) {
                                    Auser['userId'] = pDet.userId
                                    Auser['firstName'] = pDet.firstName
                                }
                            })
                        })
                        // final.push(res);
                        resolve(res);
                    }
                });
        });
    } // end of findPlayers Functions

    getSizeLimit()
        .then(findSession)
        .then(findPlayers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Expenses Data Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });

    // let findlastSession = (res) => {
    //     console.log("findlastSession");
    //     return new Promise((resolve, reject) => {
    //         let Id = [];
    //         console.log('res',res)
    //         res.results.filter((id)=>{
    //             Id.push(id.playerId)
    //         })
    //         let query = {playerId: {$in: Id}}
    //         Player.find(query,{},{})
    //             .select('-__v -_id')
    //             .exec((err, playersDetails) => {
    //                 if (err) {
    //                     logger.error("Failed to retrieve players", "playerController => findPlayers()", 5);
    //                     let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
    //                     reject(apiResponse);
    //                 } else if (check.isEmpty(playersDetails)) {
    //                     logger.error("No players found", "playerController => findPlayers()", 5);
    //                     let apiResponse = response.generate(true, "No players found", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     let final =[]
    //                     playersDetails.filter((pDet)=>{
    //                         res.results.filter((Auser)=>{
    //                             if(Auser.playerId === pDet.playerId){
    //                                 Auser['playerId'] = pDet.playerId
    //                                 Auser['firstName'] = pDet.firstName
    //                             }
    //                         })
    //                     })
    //                     final.push(res);
    //                     resolve(final);
    //                 }
    //             });
    //     });
    // } // end of findPlayers Functions
} // end of get Expenses function

module.exports = {
    getPlayerList: getPlayerList,
    getDetails: getDetails,
    getMonthlyDetails: getMonthlyDetails,
    getRakeDetails: getRakeDetails,
    getActivePlayer: getActivePlayer,
    getExpenses: getExpenses,
    createUser: createUser,

}// end exports

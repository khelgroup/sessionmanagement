import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ConfigurationService {

    api = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getAdminRewards(pageNumber, pageSize, userId) {
        return this.http.get<any>(this.api + '/rewards?userId=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    getallRewards() {
        return this.http.get<any>(this.api + '/rewards/all');
    }

    getPlayerRewardHistory(pageNumber, pageSize, userId) {
        return this.http.get<any>(this.api + '/players/getclaimrewards?userId=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    getAllRewardsForPlayers(pageNumber, pageSize, userId) {
        return this.http.get<any>(this.api + '/players/getallrewards?userId=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    getPlayerInfo(userId) {
        return this.http.get<any>(this.api + '/players/getplayerinfo?userId=' + userId);
    }

    gettableList() {
        return this.http.get<any>(this.api + '/rewards/listtables');
    }

    getfieldList(rewardName) {
        return this.http.get<any>(this.api + '/rewards/listfields?rewardName=' + rewardName);
    }

    updatePlayerProfile(data) {
        return this.http.post<any>(this.api + '/players/update', data);
    }

    addRewards(data) {
        return this.http.post<any>(this.api + '/rewards', data);
    }

    updateRewards(data) {
        return this.http.post<any>(this.api + '/rewards/update', data);
    }


    // ============================

    deleteConfiguration(data) {
        return this.http.post<any>(this.api + '/configuration/delete', data);
    }

    getTransaction(pageNumber, pageSize, userId, date) {
        return this.http.get<any>(this.api + '/users/getTransaction?configurationName=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize + '&date=' + date);
    }

    getConfigurationIds(userId) {
        return this.http.get<any>(this.api + '/configuration/getConfigurationIds?userId=' + userId);
    }

}

import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  api = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getPlayerList(pageNumber, pageSize) {
    return this.http.get<any>(this.api + '/users/getplayerlist?pg=' + pageNumber + '&pgSize=' + pageSize);
  }

  getDashboardData() {
    return this.http.get<any>(this.api + '/users/getDetails');
  }

  getSessionData(data) {
    return this.http.post<any>(this.api + '/session/getSession', data);
  }

  getExcelSessionData(data) {
    return this.http.post<any>(this.api + '/session/getExcelSessionList', data);
  }

  getMonthlyData(data) {
    return this.http.post<any>(this.api + '/users/getMonthlyDetails', data);
  }

  getActiveUserData(data) {
    return this.http.post<any>(this.api + '/users/getActivePlayer', data);
  }

  getExpensesData(data) {
    return this.http.post<any>(this.api + '/users/getExpenses', data);
  }

  getRakeData(data) {
    return this.http.post<any>(this.api + '/users/getRakeDetails', data);
  }
}

import {Component, OnInit} from '@angular/core';
import {Moment} from 'moment';
import * as moment from 'moment';
import {UserService} from '../services/user.service';
import {NotificationService} from '../services/notification.service';
import * as XLSX from "xlsx";

@Component({
    selector: 'app-session-page',
    templateUrl: './session-page.component.html',
    styleUrls: ['./session-page.component.scss']
})
export class SessionPageComponent implements OnInit {
    pageSize = 5;
    currentPage = 0;
    totalSize;
    display = 'none';
    editBlock = false;
    selectedDate: { start: Moment, end: Moment };
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];

    rows: any = [];
    filterData: any = {};

    constructor(private userService: UserService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.getCompetitionData(this.currentPage, this.pageSize);
    }

    getCompetitionData(pageNumber, pageSize) {
        const userId = localStorage.getItem('userId');
        let data = {
            pg: pageNumber,
            pgSize: pageSize,
            data: this.filterData
        };
        console.log('data', data)
        this.userService.getSessionData(data)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response, response.data);
                        this.rows = response.data.sessionDetails;
                        this.totalSize = response.data.totalRecords;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                        this.rows = [];
                        this.totalSize = 0
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getCompetitionData(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getCompetitionData(e.offset, e.pageSize);
    }

    filterMethod() {
        if (this.selectedDate.start !== null) {
            this.filterData.startDate = moment(this.selectedDate.start).format('YYYY-MM-DD');
        }
        if (this.selectedDate.end !== null) {
            this.filterData.endDate = moment(this.selectedDate.end).format('YYYY-MM-DD');
        }
        this.getCompetitionData(this.currentPage, this.pageSize);
    }

    exportsfunction() {
        let data = {
            data: this.filterData
        };
        console.log('data', data)
        this.userService.getExcelSessionData(data)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.rows = response.data;
                        this.totalSize = response.data.length;
                        const newArray: any[] = [];
                        let data = response.data;
                        Object.keys(data).forEach((key, index) => {
                            newArray.push({
                                'Session Id': data[key].sessionId,
                                'Player Id': data[key].playerId,
                                'Created On': data[key].createdOn,
                            })
                        })
                        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newArray);
                        const wb: XLSX.WorkBook = XLSX.utils.book_new();
                        XLSX.utils.book_append_sheet(wb, ws, 'All Session Searched Data');
                        /* save to file */
                        const name = 'SessionList' + this.getFormattedTime() + '.xlsx';
                        XLSX.writeFile(wb, name);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                        this.rows = [];
                        this.totalSize = 0
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }
    getFormattedTime() {
        const today = new Date();
        const y = today.getFullYear();
        // JavaScript months are 0-based.
        const m = today.getMonth() + 1;
        const d = today.getDate();
        const h = today.getHours();
        const mi = today.getMinutes();
        const s = today.getSeconds();
        return y.toString() + m.toString() + d.toString() + h.toString() + mi.toString() + s.toString();
    }
}


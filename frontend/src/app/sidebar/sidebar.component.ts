import {Component, OnInit} from '@angular/core';

declare const $: any;

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}


export const ROUTES: RouteInfo[] = [];


// {path: '/rewardmaster', title: 'Reward Master', icon: 'pe-7s-rocket', class: ''},
// {path: '/transaction', title: 'Transaction', icon: 'pe-7s-rocket', class: ''},

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    menuItems: any[];
    userRole;

    constructor() {
    }

    ngOnInit() {
        ROUTES.push({path: '/dashboard', title: 'Dashboard', icon: 'pe-7s-rocket', class: ''})
        ROUTES.push({path: '/player-detail-page', title: 'Player Detail', icon: 'pe-7s-rocket', class: ''})
        ROUTES.push({path: '/session-page', title: 'Session Detail', icon: 'pe-7s-rocket', class: ''})
        ROUTES.push({path: '/monthly-detail-page', title: 'Monthly Detail', icon: 'pe-7s-rocket', class: ''})

        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}

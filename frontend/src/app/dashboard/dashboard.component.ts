import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../services/notification.service';
import {ConfigurationService} from '../services/configuration.service';
import {UserService} from '../services/user.service';
import * as XLSX from 'xlsx';
// import { saveAs } from 'file-saver';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    rows: any = [];
    config: any = [];
    totalSize;
    currentPage = 0;
    pageSize = 5;
    display = 'none';
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '20',
            value: 20
        },
        {
            key: '50',
            value: 50
        }
    ];
    configurationId = [];

    constructor(private configurationService: ConfigurationService,
                private userService: UserService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.getDashboardData();
    }

    getDashboardData() {
        this.userService.getDashboardData()
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response', response);
                        this.rows = response.data[0];
                        this.totalSize = response.data.count;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    exportAsExcelFile(json: any[]) {
        console.log('In export excel function', json)
        let array = []
        array.push(json)
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(array);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        XLSX.writeFile(wb, 'SheetJS.xlsx');
    }

    onPageSizeChanged(event) {
        this.getDashboardData();
    }

    pageCallback(e) {
        this.getDashboardData();
    }
}

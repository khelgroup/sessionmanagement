import {Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import {NotificationService} from '../services/notification.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-player-detail-page',
  templateUrl: './player-detail-page.component.html',
  styleUrls: ['./player-detail-page.component.scss']
})
export class PlayerDetailPageComponent implements OnInit {
  @ViewChild('myTable') table: any;
  rows: any[] = [];
  expanded: any = {};
  timeout: any;
  totalSize;
  currentPage = 0;
  pageSize = 5;
  limitOptions = [
    {
      key: '5',
      value: 5
    },
    {
      key: '20',
      value: 20
    },
    {
      key: '50',
      value: 50
    }
  ];

  ColumnMode = ColumnMode;

  constructor(private userService: UserService,
              private notificationService: NotificationService) {
    // this.fetch(data => {
    //   this.rows = data;
    // });


  //   this.rows = [{
  //   id: 0,
  //   name: "Ramsey Cummings",
  //   gender: "male",
  //   age: 52,
  //   address: {
  //     state: "South Carolina",
  //     city: "Glendale"
  //   },
  //   address1: {
  //     state: "South Carolina",
  //     city: "Glendale"
  //   },
  //   address2: {
  //     state: "South Carolina",
  //     city: "Glendale"
  //   }
  // },
  //   {
  //   id: 2,
  //   name: "Ramsey Cummings",
  //   gender: "male",
  //   age: 52,
  //   address: {
  //     state: "South Carolina",
  //     city: "Glendale"
  //   },
  //   address1: {
  //     state: "South Carolina",
  //     city: "Glendale"
  //   },
  //   address2: {
  //     state: "South Carolina",
  //     city: "Glendale"
  //   },
  //
  // },
  //   {
  //     id: 3,
  //     name: "Ramsey Cummings",
  //     gender: "male",
  //     age: 52,
  //     address: {
  //       state: "South Carolina",
  //       city: "Glendale"
  //     },
  //     address1: {
  //       state: "South Carolina",
  //       city: "Glendale"
  //     },
  //     address2: {
  //       state: "South Carolina",
  //       city: "Glendale"
  //     },
  //
  //   }]
  }



  ngOnInit() {
    this.GetPlayerList(this.currentPage, this.pageSize);
  }

  GetPlayerList(pageNumber, pageSize) {
    this.userService.getPlayerList(pageNumber, pageSize)
        .subscribe((response) => {
              if (!response.error) {
                console.log('res', response);
                this.rows = response.data.results;
                this.totalSize = response.data.totalRecords;
              } else {
                this.notificationService.showNotification(response.message, 'danger')
              }
            },
            error1 => {
              console.log(error1);
              this.notificationService.showNotification(error1, 'danger');
            })
  }


  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `https://raw.githubusercontent.com/swimlane/ngx-datatable/master/src/assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  onPageSizeChanged(event) {
    this.currentPage = 0;
    this.pageSize = event;
    this.GetPlayerList(this.currentPage, this.pageSize);
  }

  pageCallback(e) {
    this.GetPlayerList(e.offset, e.pageSize);
  }
}

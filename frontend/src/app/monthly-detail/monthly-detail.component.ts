import {Component, OnInit} from '@angular/core';
import {Moment} from 'moment';
import * as moment from 'moment';
import {UserService} from '../services/user.service';
import {NotificationService} from '../services/notification.service';
import * as XLSX from "xlsx";

@Component({
    selector: 'app-monthly-detail',
    templateUrl: './monthly-detail.component.html',
    styleUrls: ['./monthly-detail.component.scss']
})
export class MonthlyDetailComponent implements OnInit {
    pageSize = 5;
    currentPage = 0;
    totalSize;
    rows: any = [];
    selectedDate;
    title = 'Monthly List';
    rangeDate: { start: Moment, end: Moment };
    defaultDate: { start: Moment, end: Moment };
    dDate = 'false';
    changePicker = false;
    filterData: any = {};
    dataJson: any = {};
    showMain = true;
    showRevenue = false;
    showActiveUser = false;
    showRake = false;
    showBudget = false;
    selectedRows: any = [];


    today = moment(new Date()).format('DD-MM-YYYY')

    constructor(private userService: UserService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.selectedDate = new Date();
        this.dataJson = this.selectedDate;
        console.log('selecteddate', this.selectedDate)
        this.getData();
    }

    getData() {
        const userId = localStorage.getItem('userId');
        let data;
        if (this.rangeDate && this.rangeDate !== undefined) {
            data = {
                data: this.rangeDate
            }
        }
        else {
            data = {
                date: this.selectedDate,
                data: {}
            }
        }
        // console.log('datajson', this.dataJson);
        this.userService.getMonthlyData(data)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data.results);
                        this.rows = response.data.results;
                        this.totalSize = response.data.results.length;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                        this.rows = [];
                        this.totalSize = 0
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    filterMethod() {
        // console.log('dataaa', moment(this.rangeDate.start).format('YYYY-MM-DD'), moment(this.rangeDate.end).format('YYYY-MM-DD'), moment(this.selectedDate).format('YYYY-MM-DD'));
        // if (this.selectedDate.start !== null) {
        //   this.filterData.startDate = moment(this.selectedDate.start).format('YYYY-MM-DD');
        // }
        // if (this.selectedDate.end !== null) {
        //   this.filterData.endDate = moment(this.selectedDate.end).format('YYYY-MM-DD');
        // }
        this.getData()
    }

    pageCallback(e) {
        this.getData();
    }

    test() {
        this.selectedDate = new Date();
        this.changePicker = !this.changePicker;
        console.log('xxxxxxxxx', this.rangeDate, this.changePicker)
        if (this.changePicker === false) {
            this.dataJson = this.selectedDate;
            this.rangeDate = this.defaultDate;
        } else {
            this.dataJson = this.rangeDate;
        }
    }

    onBudgetClick(data, name) {
        console.log('x', data, name);
        if (name === 'budget') {
          let data;
          if (this.rangeDate && this.rangeDate !== undefined) {
            data = {
              data: this.rangeDate
            }
          } else {
            data = {
              date: this.selectedDate,
              data: {}
            }
          }
          this.userService.getExpensesData(data)
              .subscribe((response) => {
                    if (!response.error) {
                      console.log('response.data.results', response.data.results);
                      this.rows = response.data.results;
                      this.totalSize = response.data.results.length;
                      this.showBudget = true;
                      this.showMain = false;
                      this.title = 'Expenses wise Monthly List';
                    } else {
                      this.notificationService.showNotification(response.message, 'danger')
                      this.rows = [];
                      this.totalSize = 0;
                      this.title = 'Expenses wise Monthly List';
                    }
                  },
                  error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                  })
        } else if (name === 'revenue') {
            this.showRevenue = true;
            this.showMain = false;
        } else if (name === 'activeUser') {
          let data;
          if (this.rangeDate && this.rangeDate !== undefined) {
            data = {
              data: this.rangeDate
            }
          } else {
            data = {
              date: this.selectedDate,
              data: {}
            }
          }
          this.userService.getActiveUserData(data)
              .subscribe((response) => {
                    if (!response.error) {
                      console.log('response.data.results', response.data.results);
                      this.rows = response.data.results;
                      this.totalSize = response.data.results.length;
                      this.showActiveUser = true;
                      this.showMain = false;
                      this.title = 'Active User Monthly List';
                    } else {
                      this.notificationService.showNotification(response.message, 'danger')
                      this.rows = [];
                      this.totalSize = 0;
                      this.title = 'Active User Monthly List';
                    }
                  },
                  error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                  })
        } else if (name === 'rake') {
          let data;
          if (this.rangeDate && this.rangeDate !== undefined) {
            data = {
              data: this.rangeDate
            }
          } else {
            data = {
              date: this.selectedDate,
              data: {}
            }
          }
          this.userService.getRakeData(data)
              .subscribe((response) => {
                    if (!response.error) {
                      console.log('response.data.results', response.data.results);
                      this.rows = response.data.results;
                      this.totalSize = response.data.results.length;
                      this.showRake = true;
                      this.showMain = false;
                      this.title = 'Rake wise Monthly List';
                    } else {
                      this.notificationService.showNotification(response.message, 'danger')
                      this.rows = [];
                      this.totalSize = 0;
                      this.title = 'Rake wise Monthly List';
                    }
                  },
                  error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                  })
        }
    }

    onBack() {
        this.showMain = true;
        this.showBudget = false;
        this.showActiveUser = false;
        this.showRake = false;
        this.showRevenue = false;
        this.getData();
        this.title = 'Monthly List';
    }

    exportsfunction(test) {
        let data;
        if (this.rangeDate && this.rangeDate !== undefined) {
            data = {
                data: this.rangeDate
            }
        }
        else {
            data = {
                date: this.selectedDate,
                data: {}
            }
        }
        if (test === 'Expenses') {
            this.userService.getExpensesData(data)
                .subscribe((response) => {
                        if (!response.error) {
                            console.log('response.data.results', response.data.results);
                            this.rows = response.data.results;
                            this.totalSize = response.data.results.length;
                            this.showBudget = true;
                            this.showMain = false;
                            const newArray: any[] = [];
                            let data = response.data.results;
                            Object.keys(data).forEach((key, index) => {
                                newArray.push({
                                    'User Id': data[key].userId,
                                    'First Name': data[key].firstName,
                                    'Month': data[key].month,
                                    'Budget Amount': data[key].budgetAmount,
                                })
                            })
                            const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newArray);
                            const wb: XLSX.WorkBook = XLSX.utils.book_new();
                            XLSX.utils.book_append_sheet(wb, ws, 'All Expenses');
                            /* save to file */
                            const name = 'ExpenseList' + this.getFormattedTime() + '.xlsx';
                            XLSX.writeFile(wb, name);
                        } else {
                            this.notificationService.showNotification(response.message, 'danger')
                            this.rows = [];
                            this.totalSize = 0
                        }
                    },
                    error1 => {
                        console.log(error1);
                        this.notificationService.showNotification(error1, 'danger');
                    })
        }
        else if (test === 'ActiveUser') {
            this.userService.getActiveUserData(data)
                .subscribe((response) => {
                        if (!response.error) {
                            console.log('response.data.results', response.data.results);
                            this.rows = response.data.results;
                            this.totalSize = response.data.results.length;
                            this.showActiveUser = true;
                            this.showMain = false;
                            const newArray: any[] = [];
                            let data = response.data.results;
                            Object.keys(data).forEach((key, index) => {
                                newArray.push({
                                    'Player Id': data[key].playerId,
                                    'First Name': data[key].firstName,
                                    'Month': data[key].month,
                                    'Year': data[key].year,
                                    'Last Login': data[key].lastLogin,
                                })
                            })
                            const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newArray);
                            const wb: XLSX.WorkBook = XLSX.utils.book_new();
                            XLSX.utils.book_append_sheet(wb, ws, 'All Active Users Data');
                            /* save to file */
                            const name = 'ActiveUserList' + this.getFormattedTime() + '.xlsx';
                            XLSX.writeFile(wb, name);
                        } else {
                            this.notificationService.showNotification(response.message, 'danger')
                            this.rows = [];
                            this.totalSize = 0
                        }
                    },
                    error1 => {
                        console.log(error1);
                        this.notificationService.showNotification(error1, 'danger');
                    })
        }
        else if (test === 'Rake') {
            this.userService.getRakeData(data)
                .subscribe((response) => {
                        if (!response.error) {
                            console.log('response.data.results', response.data.results);
                            this.rows = response.data.results;
                            this.totalSize = response.data.results.length;
                            this.showRake = true;
                            this.showMain = false;
                            const newArray: any[] = [];
                            let data = response.data.results;
                            Object.keys(data).forEach((key, index) => {
                                newArray.push({
                                    'Date': data[key].date,
                                    'Time': data[key].time,
                                    'Rake Amount': data[key].rakeAmount,
                                })
                            })
                            const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newArray);
                            const wb: XLSX.WorkBook = XLSX.utils.book_new();
                            XLSX.utils.book_append_sheet(wb, ws, 'All Rake Data');
                            /* save to file */
                            const name = 'RakeList' + this.getFormattedTime() + '.xlsx';
                            XLSX.writeFile(wb, name);
                        } else {
                            this.notificationService.showNotification(response.message, 'danger')
                            this.rows = [];
                            this.totalSize = 0
                        }
                    },
                    error1 => {
                        console.log(error1);
                        this.notificationService.showNotification(error1, 'danger');
                    })
        }
    }
    getFormattedTime() {
        const today = new Date();
        const y = today.getFullYear();
        // JavaScript months are 0-based.
        const m = today.getMonth() + 1;
        const d = today.getDate();
        const h = today.getHours();
        const mi = today.getMinutes();
        const s = today.getSeconds();
        return y.toString() + m.toString() + d.toString() + h.toString() + mi.toString() + s.toString();
    }

    exportMain(){
        let data;
        if (this.rangeDate && this.rangeDate !== undefined) {
            data = {
                data: this.rangeDate
            }
        }
        else {
            data = {
                date: this.selectedDate,
                data: {}
            }
        }
        // console.log('datajson', this.dataJson);
        this.userService.getMonthlyData(data)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data.results);
                        this.rows = response.data.results;
                        this.totalSize = response.data.results.length;
                        const newArray: any[] = [];
                        let dataa = response.data.results;
                        Object.keys(dataa).forEach((key, index) => {
                            newArray.push({
                                'Avg Revenue/Month': dataa[key].AvgRevenuePerMonth,
                                'Monthly Active Users': dataa[key].monthlyActive,
                                'Monthly Budget': dataa[key].monthlyBudget,
                                'Rake / Month': dataa[key].rakePerMonth,
                            })
                        })
                        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newArray);
                        const wb: XLSX.WorkBook = XLSX.utils.book_new();
                        XLSX.utils.book_append_sheet(wb, ws, 'Monthly Data List');
                        /* save to file */
                        const name = 'MonthyList' + this.getFormattedTime() + '.xlsx';
                        XLSX.writeFile(wb, name);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                        this.rows = [];
                        this.totalSize = 0
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }
    // onPageSizeChanged(event) {
    //   this.currentPage = 0;
    //   this.pageSize = event;
    //   this.getCompetitionData(this.currentPage, this.pageSize);
    // }
}

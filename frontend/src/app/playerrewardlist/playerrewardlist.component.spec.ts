import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerrewardlistComponent } from './playerrewardlist.component';

describe('PlayerrewardlistComponent', () => {
  let component: PlayerrewardlistComponent;
  let fixture: ComponentFixture<PlayerrewardlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerrewardlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerrewardlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

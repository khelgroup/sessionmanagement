import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LbdModule} from '../../lbd/lbd.module';
import {NguiMapModule} from '@ngui/map';

import {AdminLayoutRoutes} from './admin-layout.routing';
import {HomeComponent} from '../../home/home.component';
import {UserComponent} from '../../user/user.component';
import {TablesComponent} from '../../tables/tables.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {UpgradeComponent} from '../../upgrade/upgrade.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PlayerrewardlistComponent} from '../../playerrewardlist/playerrewardlist.component';
import {PlayerDetailPageComponent} from '../../player-detail-page/player-detail-page.component';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {SessionPageComponent} from '../../session-page/session-page.component';
import {MonthlyDetailComponent} from '../../monthly-detail/monthly-detail.component';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {DateRangePickerModule} from "@uiowa/date-range-picker";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        LbdModule,
        NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),
        NgxDatatableModule,
        NgxDaterangepickerMd,
        DateRangePickerModule,
    ],
    declarations: [
        HomeComponent,
        UserComponent,
        TablesComponent,
        TypographyComponent,
        IconsComponent,
        NotificationsComponent,
        UpgradeComponent,
        PlayerrewardlistComponent,
        PlayerDetailPageComponent,
        DashboardComponent,
        SessionPageComponent,
        MonthlyDetailComponent,
    ]
})

export class AdminLayoutModule {
}

import {Routes} from '@angular/router';
import {HomeComponent} from '../../home/home.component';
import {PlayerrewardlistComponent} from '../../playerrewardlist/playerrewardlist.component';
import {PlayerDetailPageComponent} from '../../player-detail-page/player-detail-page.component';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {SessionPageComponent} from '../../session-page/session-page.component';
import {MonthlyDetailComponent} from '../../monthly-detail/monthly-detail.component';

export const AdminLayoutRoutes: Routes = [
    {path: 'dashboard', component: DashboardComponent},
    {path: 'player-detail-page', component: PlayerDetailPageComponent},
    {path: 'session-page', component: SessionPageComponent},
    {path: 'monthly-detail-page', component: MonthlyDetailComponent},
];
